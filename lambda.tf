resource "aws_lambda_function" "put-text" {
  filename      = data.archive_file.put-text.output_path
  function_name = "${var.prefix}-putText"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "newtext.lambda_handler"
  runtime = "python2.7"

  environment {
    variables = {
      DB_TABLE_NAME = aws_dynamodb_table.iaac-demo-1-texts.name
      BUCKET_NAME = aws_s3_bucket.mp3.bucket
    }
  }
  timeout = 300
}

data "archive_file" "put-text" {
  type        = "zip"
  source_file = "${path.module}/mnhacknightv1/newtext.py"
  output_path = "${path.module}/zip/put-text.zip"
}


resource "aws_lambda_function" "get-text" {
  filename      = data.archive_file.get-text.output_path
  function_name = "${var.prefix}-getText"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "gettext.lambda_handler"
  runtime = "python2.7"

  environment {
    variables = {
      DB_TABLE_NAME = aws_dynamodb_table.iaac-demo-1-texts.name
    }
  }
  timeout = 300
}

data "archive_file" "get-text" {
  type        = "zip"
  source_file = "${path.module}/mnhacknightv1/gettext.py"
  output_path = "${path.module}/zip/get-text.zip"
}