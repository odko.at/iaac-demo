output "web-bucket-domain-name" {
  value = "http://${var.prefix}-web.s3-website.${var.region}.amazonaws.com"
}
output "api-gw-url" {
  value = aws_api_gateway_deployment.prod.invoke_url
}