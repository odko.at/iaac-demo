resource "aws_s3_bucket" "web" {
  bucket = "${var.prefix}-web"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_s3_bucket" "mp3" {
  bucket = "${var.prefix}-mp3"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_s3_bucket_object" "index" {
  depends_on = [aws_s3_bucket.web]
  bucket = "${var.prefix}-web"
  key    = "index.html"
  source = "${path.module}/mnhacknightv1/index.html"
  acl = "public-read"
  content_type = "text/html"

}

resource "aws_s3_bucket_object" "script" {
  depends_on = [aws_s3_bucket.web, aws_api_gateway_deployment.prod]
  bucket = "${var.prefix}-web"
  key    = "scripts.js"
  content = "<h1>Hello world</h1>"
  # source = "${path.module}/mnhacknightv1/scripts.js"
  acl = "public-read"
  content_type = "text/javascript"
}

resource "aws_s3_bucket_object" "css" {
  bucket = "${var.prefix}-web"
  depends_on = [aws_s3_bucket.web]
  key    = "styles.css"
  source = "${path.module}/mnhacknightv1/styles.css"
  acl = "public-read"
  content_type = "text/css"
}
