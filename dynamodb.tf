resource "aws_dynamodb_table" "iaac-demo-1-texts" {
  name         = "texts"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "id"
  attribute {
    name = "id"
    type = "S"
  }
}